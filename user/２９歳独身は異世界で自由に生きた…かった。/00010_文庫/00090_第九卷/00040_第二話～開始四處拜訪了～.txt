「吶，夫君。你真的要帶臣妾去嗎？」

「是啊，我要帶你去。」

「那個，臣妾還是留在幸運草比較好吧？我很擔心有身孕的瑪爾和美兒琪娜，而且俘虜說不定會造反。」

她從剛才開始就一直這樣。不曉得在擔心什麼，平時的從容沉著也消失無踪。不，其實我已經察覺原因了。

「楠葉。」

我伸出的手貼在神情不安的楠葉臉上，並直視她的雙眼。因為楠葉的下半身很龐大，身高比我還高，不過我伸手還是碰得到她的臉啦。

「不管周圍的人怎麼說、怎麼想，你都是我老婆。關於這一點，你完全不用擔心，要是有人敢說什麼，我一定會將對方揍扁。」

「夫君……」

(插圖002)

楠葉似乎對自己身為阿爾嘉尼亞的外表，產生了些許自卑感。但是包括我在內，家裡沒有一個人在意這件事。雖然平時生活在幸運草裡的都是獸人、半人馬、里札多曼和鬼人族，她本人並沒有表現出在意的樣子，可是對於去都是人類的密斯克洛尼亞王國和卡倫狄魯王國，她的態度卻始終舉棋不定。像現在也是一直嘮叨個不停。

「這麼親熱的舉動，我到現在還是模仿不來呢。」

「呼～好熱啊。」

我和楠葉這一連串的對話，被在一旁看好戲的妮菈和莉法娜揶揄。史黛拉則是笑眯眯地不作聲。嗯，我承認剛才不小心進入兩人世界了。我都承認了，拜託你們不要用溫情的眼神看著我。

「咳咳……你們都準備好了嗎？」

「有什麼好準備的？行李不是都在你身上嗎？」

「如你所見，我準備好了。」

妮菈兩手空空，莉法娜則是一如往常的獵人裝備。以造訪城市來說，這樣的裝備有點太誇張了。

「莉法娜，不要帶弓和箭筒啦。那樣很佔空間，而且入城時也得交給別人保管。」

「是嗎？那好吧。」

莉法娜乖乖地把弓和箭筒交給我。至於小刀或是短劍之類的，還是讓她帶在身上好了。有我和妮菈在應該不會有問題，楠葉則是沒有武器也很強。

將莉法娜的弓和箭筒收進倉庫後，我打開轉移門。前往地點是卡倫狄魯王國的宅邸。好一陣子沒去了，不曉得梅蓓爾和杰克先生好不好？

「那我們先去王都阿爾芬的宅邸吧。」

「哎呀，不是從密斯克洛尼亞王國開始嗎？」

「因為我想在那邊配置人手，所以先從卡倫狄魯王國開始。」

說完，我鑽過轉移門。經過瞬間的飄浮感和昏暗後視野切換，許久不見的宅邸門廳映入了眼簾。屋內還是一樣打掃得一塵不染呢。

繼我之後是莉法娜、妮菈、史黛拉，接著過了一會兒，楠葉也從轉移門現身。

「儘管體驗過好幾次了，我還是很不習慣這種感覺耶。」

「我懂你的意思。那種感覺真教人毛骨悚然。」

「我倒是滿喜歡的。」

「唔，臣妾不太在意呢……對了，這裡是哪裡？」

正當莉法娜等人各自發表對轉移門的感想時，杰克先生從屋內現身了。認出我的他，睜大雙眼，接著泛起深深的笑意。

「老爺，歡迎回來。見到您身體健壯，真是再好不過了。」

「抱歉這麼久沒回來。我想謁見國王，可以麻煩你安排嗎？」

「請交給我。梅蓓爾！」

杰克先生一喊，隨即就聽見「啪噠啪噠」的腳步聲從屋內傳來。

「是！啊！大志大人？幸好您平安無事！」

從屋內現身的梅蓓爾瞪大雙眼，跑了過來。一陣子不見，她是不是長高了啊？

「梅蓓爾，我要去王宮預先通報，你可別怠慢招呼。」

「是，請儘管交給我。」

一陣子不見，梅蓓爾也成為獨當一面的女僕啦。感覺她的舉止動作變得幹練許多。

「各位，這邊請。」

目送杰克先生快步離開宅邸後，我們前往會客室。由於這棟房子是配合人類的體型所興建，因此對楠葉來說似乎有些狹小。

我們分別坐在沙發上，至於楠葉則是稍微騰出空間，擺上她愛用的坐墊，讓她坐在那上頭。因為她沒辦法坐人類用的椅子嘛。

不一會兒，梅蓓爾帶著和人數相符的茶水、糕點來到了會客室。看著她將茶端到面前，我放鬆了精神。

「好久不見，你是不是長高了啊？」

「是的，我長高了一點。」

我們聊起彼此的近況，度過一段和睦的時光。老婆們好像對我在這間屋子生活時的事情很感興趣，向梅蓓爾問了不少問題。嗯，希望她不要提起我和瑪爾、芙拉姆之間赤裸裸的往事。

聊著聊著，杰克先生回來了，於是我們便動身前往王宮。

「這是什麼啊？」

「因為如果是一般馬車，會對那位夫人造成不便。」

「原來如此，說得也是。」

杰克先生調來的馬車並非車廂型馬車，而是沒有屋頂的大型馬車。嗯，我曾經搭過這種馬車好幾次。像是殲滅半獸人聚落凱旋歸來時，還有參加士氣高昂的典禮時。

我記得曾經在原本世界的新聞中，見過英國王族搭乘這種馬車，向人民揮手。如果是這個大小，確實連楠葉也能順利坐上去。

「那我也打扮得正式一點好了。」

我離開一會兒，穿上昨天從佩倫小姐的店裡找來的秘銀製盔甲，腰間則插上對神武裝的極光劍。劍鞘是昨晚睡前做好的。

「嗯，看起來威風凜凜呢。」

「嗯，這副武將裝扮真是適合夫君。」

「嗯，真的很帥氣呢。」

「這個嘛，畢竟我是靠武藝成名的，所以多少有點樣子啦。」

請不要接連稱讚我，這樣我渾身不自在。

在近衛騎士的引領下，我們所搭乘的馬車行駛在王都阿爾芬裡。因為是大方行經街道，我們的模樣自然也就暴露在眾人的目光之下。

「喂，你看那個。」

「那不是勇者大人嗎？好久沒見到他了。還有卡瑠妮菈公主和……那是什麼？」

「那應該不是人類吧……雖然臉長得非常美，不過那副身體……」

「我以前就聽說勇者大人有怪食癖，原來是真的啊。」

完全就是口無遮攔。我是不在乎別人怎麼說我，但是居然把楠葉當成怪東西，小心我真的宰了你們喔。

「夫君，忍著點。」

「可是他們那樣說你耶。」

「臣妾知道夫君很喜愛臣妾，不必理會那些不入流之輩。」

語畢，楠葉在我臉頰上一吻。我也同樣親吻她的臉頰，然後彼此相視而笑。嗯，說得也是。雖然很火大，不過既然楠葉都這麼說了，我就忍耐吧。

(插圖p059)

「你們兩個是不是忘了我們也在？」

「居然在我們面前放閃。」

妮菈和莉法娜露出有些不滿的表情。有什麼辦法呢，因為你們兩人坐在對面的位子上，距離沒有近到可以馬上親吻嘛。

「呵呵，這是特權。」

嗯，楠葉小姐，麻煩你也不要用那副耀武揚威的表情摟住我的脖子。瞧，她們兩人的笑容都變得有點僵硬了。和平共處、和平共處。話說，我在決定這次的成員時並沒有考慮到合不合的問題，莫非組合失敗了？

不過，真希望她們有一天能夠習慣，然後彼此和睦相處。要是這次的事情，能順利成為她們感情變融洽的契機就好了。

「沒辦法，就當作是禮讓前輩好了。」

「對了，我不知道你們當初是怎麼認識的耶。之後可以請你好好地告訴我嗎？」

「可以啊。臣妾對夫君和莉法娜小姐的相識過程也很好奇。」

「不需要加小姐兩個字啦。我也可以直呼你的名字嗎？」

「你也可以用暱稱叫我喔。」

完全無視我的擔心，這群女孩子開始和樂融融地聊起天。太好了，看來我不必因為尷尬的氣氛而胃痛了。

聊著聊著，馬車穿越第二城牆，進入了貴族區。一進入貴族區，路上行人變得零星許多，也沒有人會用大聲量說三道四。

---

來到這裡後，王宮就近在眼前了。

「這裡是你久違的老家呢。」

「既然娶了我，就某方面而言，這裡也是你家喔。」

「房子這麼大，承受不起啊。我覺得現在住的房子大小比較剛好。」

「是嗎？我倒覺得今後家人還會增加，換更大一點的房子也好。反正土地還有那麼多。」

唔，這麼說來也有道理。畢竟今後孩子的數量會繼續增加，也需要雇請傭人之類的。這麼一來，現在的房子確實有可能太小了。

「這方面的事情也一併商量看看好了。」

「那就由我來跟父親大人提這件事好了。我想應該能夠找到一群一流的建屋工匠。」

「好啊，那就請工匠把房子蓋在某個地方，然後轉移過去好了。」

只要發動空間魔法和倉庫，就能將房子連同地基一起遷移。因為我已經弄壞好幾棟破屋，充分練習過了！再說建築方面的資金也很充裕。


我們沒有接受盤查就直接通過王宮大門，在近衛兵的帶領下踏進宮內。由於王宮內通道也很寬敞，因此就連楠葉也能順利通行。只不過，來到謁見廳附近後就變成一般的通道，寬度顯得有些狹窄。

我們被帶往的地方，是過去我和卡倫狄魯國王會談過的會議室。我把幾張椅子拿開，放上坐墊，當成楠葉的座位。史黛拉好像有什麼事情要辦，一抵達王宮就行了個禮，去了別處。

「這種時候，是不是應該等國王來了再坐下比較好？」

「不必在意那麼多，況且卡倫狄魯王國和勇魔聯邦又不是主從關係。再說，在權力關係上，勇魔聯邦又或者應該說是你，地位反而在上不是嗎？」

「我唯獨對暴力是挺有自信的。」

「說什麼暴力……」

「夫君是力量的化身這一點，是眾所周知的事實。臣妾雖然不曾現場目睹，不過聽說他幾乎是憑一人之力，消滅了數萬魔物。實際上，他在大樹海裡，也在閑暇之餘殲滅了我等避之唯恐不及的凶猛魔獸。人類會對他懷抱畏懼之心也是可以理解的。」

楠葉用莫名得意的表情，對臉頰綳緊的莉法娜這麼說。說起這個楠葉，她雖然有思慮縝密的一面，卻也有力量信奉者的性格。

「唔，這個嘛，大志的強大確實有些異常。畢竟他之前還跟戰神大人稀鬆平常地互毆過。」

「戰神大人？你是說戰神迪奧嗎？」

「是啊。我為了得到他的祝福，到他的御座去踢館，結果算是打贏了。」

「如果不是你，我就把這話當成吹牛，嗤之以鼻了……」

這次換成妮菈的臉頰緊繃。哎呀，這真的不是在吹牛啦。雖然只要去神殿確認一下應該就能證明，但我現在實在不太想接近神殿。

閑聊一陣後，卡倫狄魯國王和重臣們進入室內。我從位子上站起來，行了一禮後再次坐下。

「恭喜你這次平安歸來，」

「謝謝。這次過來，是想向你報告我已經回來，還有我擊退了攻打領都幸運草的賊寇們，岳父大人。」

「關於那件事……」

卡倫狄魯國王揉捏眉間的皺紋，望向重臣。在國王的注視下，重臣臉色蒼白地冒著冷汗，開口說道：

「那個……我們卡倫狄魯王國的貴族也有一部分……加入了討伐軍。是在靠近戈培爾王國國境的位置擁有領地的地方貴族。」

「那就沒辦法了。不過，既然對方向我挑釁，我當然會在確認事實關係後展開報復。如果卡倫狄魯王國無法出手，那就由我來動手吧。這件事情就當成是勇魔聯邦，和對勇魔聯邦出手的地方貴族們之間的問題。」

我這次的方針是「笑眯眯及盡可能地表現和善」。因為過去我對卡倫狄魯王國的態度一直都很冷淡嘛，考慮到我的老婆妮菈的立場，我今後打算把卡倫狄魯王國——應該說是卡倫狄魯王室也當成自己人看待。


可是我說你們，怎麼見到我和藹可親的笑容後，臉色反而變得更蒼白？我沒有別的意思喔。呵呵呵……我沒有在生氣喔。

「請問……你想報復到什麼程度？」

「這個嘛……因為他們要求我把勇魔聯邦的領民和領地全數交出，所以我提出相同要求應該很合理吧？」

我對著卡倫狄魯王國那些人露出完美的笑容。不知為何，重臣大叔們個個面如槁木。究竟是為什麼呢～

由於諸位重臣好比被蛇盯上的青蛙一樣僵住，我於是將視線轉向老婆們。妮菈陷入沉思，楠葉點頭，莉法娜則是聳了聳肩。

「以牙還牙是常理，理應要有這種覺悟才是。」

「我是沒什麼意見。只不過，以森林的規矩來說，強盜不僅會被沒收個人財產，還會遭到放逐。」

聽完楠葉和莉法娜的意見，妮菈大概也整理好自己的意見，開口說道：

「將該貴族家系廢除爵位，領地歸王室直轄，財產則全數讓渡給勇魔聯邦作為賠償……這樣的處置應該可以吧？」

妮菈提出的方案，是我們四人的意見中最輕微的。楠葉不悅地蹙起眉頭。

「這樣會不會太寬容了？」

「或許是這樣沒錯，不過大志大人並不需要遠離大樹海、散落他處的領地吧？而且他應該也不希望為了泄憤，將領地內破壞殆盡、折磨無辜的人民，所以我認為也只能採取這樣的折衷做法了。」

嗯，妮菈說的確實沒錯。我只想教訓對幸運草動武的貴族，的確不想恨烏及屋的過度折磨當地民眾。


再說，即使誅殺身為領地首領的地方貴族取代領主，也沒辦法按照原本的方式營運領地。因為也許有人發誓要效忠現在的領主，也可能有很多已經建立起良好關係的御用商人、仕紳、鄉間武士等等。不過，鄉間武士可能大致都被瑪爾的地雷炸死就是了。

總之，那些人未必會唯唯諾諾地順從突然登上領主之位的人，視情況還可能會頑強抵抗。考慮到這一點，將整塊領地奪走並不是好主意。

「我想就採用妮菈的方案好了。只不過，既然我向卡倫狄魯王國讓步了，希望你們也能夠多多關照。」

「請問這話是什麼意思？」

臉色稍微恢復血色的重臣，一邊用手帕擦拭額頭的汗水，一邊詢問。

「我在這次的戰爭中抓了大量的俘虜，他們所消耗的糧食數量不容小覷。如果有多餘糧食，希望你們能夠以適當的價格讓給我。」

卡倫狄魯國王聽了我的要求，和其中一名重臣交換眼神，確認對方點頭後才重新面對我。

「不以等價購買也無妨，我們會盡可能地給予支援。」

「我並不打算敲詐你們，所以一定會付錢。但如果你們沒有多餘的糧食可以賣也沒關係。」

我也可以不透過國家，以個人名義到處向城鎮、村莊收購。而且最糟的情況，只要我努力打獵，想要多少肉應該都不成問題。

「親愛的，人情上的借貸過大也會妨礙建立健全的關係喔。再說，卡倫狄魯王國本來就欠你很大的人情，你就坦率接受吧。」

「唔，是這樣嗎……？那麼，我就恭敬不如從命了。」

也對，欠別人太多人情，的確是會擔心對方不曉得會以此為要脅，向自己提出何種要求。這種心情我懂。既然如此，我就乖乖聽妮菈的話好了。

─☆★☆─

與卡倫狄魯王國之間的交涉，基本上是以採納妮菈的意見的形式進行，過程和以前相比和平許多。卡倫狄魯國王和重臣們看妮菈的眼神，簡直就像見到救命女神似的，實在令人印象深刻。


之後，我們又針對具體的糧食支援內容、地方貴族的處分方法、建設新屋等各種案件，完成了商議。

「夫君，你會不會對他們太仁慈了？」

接走提著行李等待的史黛拉後，楠葉在回程的馬車途中，有些不滿地向我問道。就楠葉的觀點，她大概希望我以更強硬的態度去跟對方交涉吧。

「過去，我對卡倫狄魯王國的態度一直都很冷淡，可是如今妮菈嫁給了我，她應該需要實績來證明自己有幫忙改善我與卡倫狄魯王國之間的關係。況且，對方是自己人，對自己人那麼嚴苛也無濟於事啊。」

說完，我望向妮菈，只見她泛起微笑。

「有這麼一位願意替我著想的丈夫，我真是太幸福了。」

「再說，我們雙方已經決定攻打反叛的地方貴族時，我也會出馬，所以我的名聲並不會因此受損。到時我一定會盡情大鬧的。」

針對壓制參與攻打幸運草的地方貴族領地一事，目前程序已大致底定。以作戰流程來說相當單純。

也就是利用我的轉移門，將卡倫狄魯王國所編制的精銳鎮壓部隊送往地方貴族的領地，由我粉碎防衛戰力和主要防衛設備後，再一口氣壓制據點。

由於我一個人所能做的頂多就是破壞一切、夷為平地，因此如果要確實壓制領地，就少不了卡倫狄魯王國的兵力。

「唔……既然夫君這麼說，臣妾就不再多言了。」

「哈哈哈，我很高興楠葉你這麼為我的名譽著想喔。不過呢，比起名譽，我只要能夠和你們安靜地好好過日子就夠了。雖然偏偏有很多人要來搗亂。」

像是戈培爾王國的王太子、沃爾特等人，還有這次攻打幸運草的那群笨蛋……真是的，明明只要他們不理我，我就會窩在大樹海裡，他們卻偏偏故意要來糾纏我。

因為總不能一直乖乖挨打，所以我也只好出去大鬧了。真麻煩。

「大志，接下來是去密斯克洛尼亞王國嗎？」

「這個嘛……嗯～」

聽了莉法娜的問題，我仰望天空。差不多快中午了，還是先吃飯休息一下，再去密斯克洛尼亞王國好了。其實我很想帶楠葉和莉法娜在王都阿爾芬裡四處參觀……可是密斯克洛尼亞王國對非人類種族的偏見比較輕微，實在沒必要故意留在卡倫狄魯王國，打壞自己的心情。

「我們回宅邸吃飯、休息一下，之後再去密斯克洛尼亞王國好了。」

「我也覺得這樣比較好……啊，對了，史黛拉，最後結果怎麼樣？」

「是的，妮菈公主，結果剛剛好。」

表示贊成的妮菈一問，史黛拉便舉起一個相當老舊的皮革包包。那個包包不是很大，大概跟小學生的書包差不多。外觀和身穿筆挺女僕服的史黛拉感覺不太搭調。

可能是感受到我的視線，史黛拉撫摸著皮革包包，一邊開口說道：

「這個包包是寶物袋喔，可以裝進大約一輛馬車的貨物。」

「喔，這麼說來是魔法道具啊。我記得寶物袋用買的要花不少錢吧？」

「是啊，一個寶物袋的價格差不多都可以蓋房子了。」

「是喔，既然這樣，以後說不定可以做寶物袋來賣錢。」

因為我以前曾經做過一個給瑪爾使用，現在也還記得怎麼做。雖然需要強大魔物的皮或胃袋作為材料，不過在大樹海裡不愁沒有材料來源。

「……你會做嗎？」

「嗯？會啊。我以前就做過一個送給瑪爾。」

「……我開始覺得頭痛了。」

妮菈按著太陽穴來回揉著。史黛拉也感覺一臉苦笑耶。楠葉和莉法娜倒是一臉驚訝。

「怎麼了？」

「你還問呢，我從來都沒有聽說過有人能夠自制寶物袋。寶物袋的做法失傳已久，是極少能夠在遺跡等處找到的珍貴物品耶。」

「原來是這樣啊。這個嘛，對普通人來說，這東西的確很方便。」

我一邊回想在大森林體驗過的鹽巴交易之旅，一邊點頭回應。當時的確要是不能使用寶盒，就得一路背著沉重的貨物了。這麼一來，移動速度就會下滑，而且人數如果增加，消耗的糧食也會跟著變多。能夠縮小並減輕大量行李帶著走的寶物袋，對許多人來說應該是令人垂涎的物品。

「嗯……仔細想想，這個東西也能用在買賣上，我看有時間的話，不如就做幾個備用好了。反正街道警備隊和獵人們應該也用得著。」

「……就是啊。」

那個微妙的停頓是怎麼回事？應該說，你為什麼要一副好像很厭惡自己似的揉捏眉間的皺紋啊？你今天是第幾次做那個動作了？

「怎麼了？」

「我只是有點陷入自我厭惡的情緒裡……嗚嗚。」

「自我厭惡？」

這番突如其來的發言令我一頭霧水。楠葉和莉法娜好像也無法理解她為何會冒出這種話，露出狐疑的表情。

「你對瑪莉艾爾……不，恐怕就連我，只要我在不背離倫理的範圍內向你請求，你就會替我實現願望對吧？」

「咦？這個嘛……也得把你的願望說來聽聽才知道。」

我的回答大致是肯定的。只要那個請求在我能力所及範圍內，而且不會危及所有人，我就會努力達成。

「我想也是。唉……我真是服了瑪莉艾爾了。」

這次她大大地嘆了口氣。啊，她用雙手捂住臉了。她到底怎麼了啊？

「吶，她在沮喪什麼啊？」

「不知道。雖然她提到了瑪爾，可是我完全無法理解。」

「唔嗯……？難道說，你想要利用大志的力量？」

妮菈輕輕點頭回應莉法娜的問題，之後楠葉也一副了然地面露苦笑。

「到底是怎麼回事啦。」

「意思是，夫君的力量太強太。只要巧妙地利用夫君，若要得到這世上的一切亦非難事。」

「這話會不會有點太高估我了？」

我一個人哪有那麼大的力量呢……沒有吧？

「臣妾反倒覺得夫君太小看自己。只要夫君想做，應該不消一星期就能毀滅一個國家吧？」

「我是覺得沒有那麼容易……」

還是來想像一下好了。


假使要毀滅一個國家，我首先應該做的就是徹底擊潰首領。這一點，只要我殺進王宮，仔細將其夷為平地就能辦到。如果擔心有地下通道，只要在夷為平地後，在上面用地魔法仔細翻土就好。

接著是以相同方式，擊敗各個主要都市的首領。若能同時對主要街道和穀倉地帶進行破壞是最好的。只要將決策機關的首領全數打倒，之後就算置之不理，國家也會自行滅亡。一旦行政服務停滯，治安便會轉眼惡化，然後隨著地域因四處形成非法地帶而分裂，最後將無法再保有國家的形式。


不僅如此，只要破壞街道和穀倉地帶，經濟活動也會被迫大幅縮小，糧食不足則會加速治安惡化和分裂地域之間的對立。屆時，人們為求糧食而相爭的地獄，想必將會在這個世上出現。

而且，這個世界上還有魔物這項威脅。這個世界的軍事力量本來就不是用來對付人類，而是為了魔物而存在。人們之所以能夠相對安全地在各都市間移動，是因為有軍隊定期撲滅在街道附近橫行的魔物群。一旦軍隊失去機能，負責生產糧食的多數村落很可能會被消滅。


當然，各國不可能會認同我四處大鬧的作為，就這麼袖手旁觀。他們大概會為了討伐我而派出王牌——勇者吧。因為派遣大軍對付能夠單獨殺死數以萬計之魔物的我，完全是一種愚蠢的行為。

然而，各國的勇者要捕捉我幾乎是不可能的。我能夠利用空間魔法瞬間移動至遠處，而且還擁有超音速的飛行魔法。要把我引誘出來，讓我無法使用空間魔法、飛行魔法逃走，然後將我徹底打倒，這種事情恐怕只有眾神才辦得到吧？


雖然可能也有攻打幸運草這一招，不過只要我事先將居民移至他處，之後再開始大鬧，這個弱點也就不存在了。

唔，避難所啊。我之後是不是應該挑個好地點，偷偷在那裡蓋避難所呢？

「看夫君的表情，就知道是想到了什麼不正經的點子。」

「才沒有那回事喔……不過，我的確做出了只要我想，就能輕易毀滅一兩個國家的結論。不過需要事前準備就是了。」

「對吧？所以，一旦受到那樣的夫君寵愛，難免會讓人產生誤解，感覺好像是自己擁有強大力量似的。卡瑠妮菈就是察覺到自己差點變成那樣了。」

「……是喔？」

感覺似懂非懂耶。意思是，她因為發現自己差點狐假虎威，所以才厭惡自己嗎？可是這和瑪爾有什麼關係？

「夫君，瑪爾小妞可是一個非常傑出的人喔。因為她一直和那樣的夫君生活在一起，又最受夫君的寵愛，卻完全沒有因此而驕矜自喜，堅定不移地支持著夫君。」

「哪有那麼誇張……不，仔細想想好像是事實……？」

由於我本身不喜政治方面的話題，所以基本上全都交給瑪爾去處理。仔細想想，倘若瑪爾有心誘導我，我搞不好早就滅了卡倫狄魯王國和戈培爾王國了。

「話說回來，妮菈根本沒必要沮喪啊。既然你都自己察覺到，還有了那種想法，那就沒什麼好擔心了。」

「可是我……」

「你聽我說，我也不是一個什麼都沒在想的笨蛋，我也有自己的想法。如果我有什麼意見，就會如實地表達出來。再說，從剛才那番話聽來，你們簡直就把我當成可以輕易玩弄於股掌間的笨蛋嘛。」

我惡狠狠地瞪著三人。結果，她們先是表情一愣，之後就互相望著彼此，泛起笑意。

「說得也是，那些話的確對夫君很失禮。」

「也對，大志說得沒錯。」

「就是啊就是啊。」

不過真要說起來，我其實很清楚自己是個笨蛋，而且老實說要是瑪爾或緹娜、楠葉、妮菈，還有卡蓮和愛兒米娜小姐要騙我，我有信心一定會上當！冷靜想想，有一半的老婆都能夠把我騙得團團轉呢。

算、算了，如果她們要團結起來騙我，那我就默默地被騙好了。但我覺得她們應該是不會騙我啦。

「所以，你就不要操無謂的心，要是有什麼想說的或想拜託我的事情，就儘管開口。如果我覺得辦不到就會直說，辦得到的話就會去做，所以你就別再露出那麼無精打采的表情啦。」

「是，親愛的。」

「很好。」

妮菈抬頭微笑的笑容猶如寶石一般眩目……嗯，卡倫狄魯王國的寶石這個偉大的名號果真不是浪得虛名。我深深體認到這一點。

─☆★☆─

在宅邸用完午餐稍事休息之後，我們移動前往密斯克洛尼亞王國的王都庫倫。雖然不是了若指掌，不過畢竟我也頻繁進出過這座王宮……應該說是被迫進出，所以我靠這張臉就進城門了。


之後，要求晉見這個國家的王妃，實質上的統治者，同時也是我岳母的依露歐妮·布朗·密斯克洛尼亞的我們，被帶往會客室，不久就見到了我的岳父母，也就是國王夫妻。

「好久不見，我平安歸來了。」

「是，歡迎你回來。見到你平安無事真是太好了。光是你能夠四肢健全地通過神的試練回來，就已經非常了不起了。」

依露歐妮——依露小姐用一貫笑盈盈的表情慰勞我。反觀岳父艾爾文大叔則是交抱雙臂，一臉不悅地瞪著我。

「怎樣啦？」

「哼，居然剛結完婚就不見人影。雖然嘴巴上說是接受什麼神的試練，但其實你只是承擔不了責任，逃走了吧？」

「你說什麼……」

這話實在令人生氣。你這個臭大叔，你可知道為了回來，我一路上吃了多少苦頭嗎？我本想拍桌大吼，卻有兩人比我早一步拍桌。

「你知道大志是抱著多大的決心回來嗎！你這個小鬼！」

「就算你是國王，我也不允許你愚弄夫君！你這個小兔崽子！」

火大的莉法娜的拳頭在風魔力的纏繞下，讓高級的木桌產生裂痕，接著同樣火大的楠葉有如大槌的攻擊臂，將產生裂痕的桌子一如字面地打得粉碎。

面對眼前瞬間發生的狀況，我和密斯克洛尼亞國王夫妻不禁錯愕地僵在原地，就連周圍的護衛騎士和女僕們也是一樣。妮菈也張口愣住了。

「大志他啊！他可是在一身襤褸，連力量也幾乎喪失的狀態下，逃到大森林裡！他全身上下就只有破爛的內衣褲和襯衫，不管是鞋子還是武器什麼都沒了，就連魔法也幾乎無法使用，只拿著一根木棒在強大魔物橫行的大森林裡徘徊！儘管身處那樣的狀況，他還是心繫妻子們，並且為了下次遭遇眾神時要將其擊敗而拚命取回力量，回到大家身旁！他明明大可選擇拋棄一切，隱居大森林中，卻還是不惜和眾神敵對，也要為了妻子們回來！然而你卻說他逃走？開什麼玩笑！」

沒錯，非常好，莉法娜你再多說幾句。

「就是啊！夫君他啊，雖然確實很好色、懶散、馬虎又自我中心！但是！他擁有不管對方是誰都能平等以待、締結友誼、給予關愛的胸襟！不只是被人類稱為魔物，不得不隱居大樹海的我等阿爾嘉尼亞！還有以前在卡倫狄魯王國受虐的獸人、在戈培爾王國受戰火燒灼的半人馬，以及在這個密斯克洛尼亞王國被當成違法奴隷的女孩們！夫君擁有將眼見所及、即使不出手相助也不會有人責備他的弱者，一一親手拯救、擁入懷中的氣度！相形之下！就算兩個心愛的女兒都嫁人了，但是像你這種一直嘮嘮叨叨、囉哩囉嗦、懷恨在心的人，根本就是度量狹小的愚夫！」

嗯，楠葉則是先損我一頓再把我捧上天。算了，沒差啦。

被兩人用手指著大罵的岳父大人，依舊半張著嘴，神情愕然。我應該趁這個時候先發制人。

「啊～謝謝你們替我說話，不過你們還是冷靜一點，坐下吧。」

「啊……嗯，抱歉。」

「……哼，既然夫君這麼說，臣妾只好遵命了。」

聽了我的話，莉法娜大概是突然回神了，只見她臉色有些蒼白地坐下，楠葉則是一臉不滿地坐在坐墊上。我看著她們就座，並把變成碎片的桌子收進倉庫，然後拿出兩個高度差不多的圓形木塊並在一起，當成桌子用。


儘管有些粗獷，但是可以保證絕對堅固。不過，如果遭到楠葉粗壯的攻擊臂毆打，可能還是會粉碎啦。

「啊，要怎麼說呢……抱歉，因為她們很愛我。」

「呵呵……看來是如此呢～親愛的，你這樣不行啦～你應該最清楚神的試練有多嚴酷了，不是嗎？再說，瑪爾和緹娜都已經嫁為人婦，你也差不多該放手了～」

在依露小姐的勸導下，大叔一臉尷尬地沉默不語。因為這個時候再繼續刺激他，只會讓事情變得膠著，於是我決定無視沉默的大叔。

「總之，我想要簡單扼要地說明整件事情……可以請你們聽我說嗎？」

說完，我望向在房裡待命的護衛騎士和女僕們。方才神情錯愕的他們似乎也已恢復正常。

「放心啦～你們幾個，不可以把在這裡聽到的事情泄漏出去喔～要是泄漏出去，我可是會處分你們所有人喔～如果不想聽就出去吧，快走、快走。」

依露小姐笑盈盈地說完，女僕們隨即慌忙離開房間，只有兩名騎士留下來。

「好了，我幫你把人支開了。」

「那麼，我就依序開始說了。莉法娜，如果你有要補充的事情就開口。」

「知道了。」

得到莉法娜的同意後，我從在卡倫狄魯王國的王都阿爾芬遇見大地女神蓋娜開始，依序說出一切。蓋娜的警告、為試練擬定的對策、與眾神之戰，然後是敗逃。

失去一切後，和利亞一起逃到大森林，在那裡徘徊。遇見愛兒米娜小姐、在精靈村落生活、鹽巴交易、在大森林遇見的異種族。還有為了尋找擬神格，四處造訪遺跡的事情。

得到擬神格、取回力量之後，開始到處拜訪神的御座。與眾神邂逅、對話，與軍神交戰。接著是返回幸運草，賊軍的末路，最後是戰後處置。

「嗯，大概就是這麼回事。至於和卡倫狄魯王國方面的商談結果，就跟剛才提到的一樣。」

包括莉法娜的補充在內，將整件事說明過一遍的我，喝了一點裝在陶製高腳杯中的淡蟻蜜酒來潤喉。這個蟻蜜酒，是以在幸運草與我們共生的安提魯族所提供的蟻蜜釀成。風味絕佳，不比蜂蜜酒遜色，聽說也有一定的產量。目前正在討論要不要將其當成幸運草的特產。

因為我在第二天午餐時有喝到，因此非常喜歡，於是就請人分了一個小木桶的量給我，然後稀釋成我偏愛的口味，放進倉庫裡。如果直接喝，對我來說有點太甜。

「雖然這話出自大志的口中，內容還是令人難以置信……不過，我想恐怕都是事實吧。」

「就是啊～如果今天立場對調，我也會這麼認為，但是很遺憾，這些全部都是事實。」

「也對～如果要撒謊，你就會編一個更容易讓人相信的謊言了～……這麼一來就傷腦筋了呢～」

依露小姐一副真的很困擾地把手貼在臉頰上，偏了偏頭。

「怎麼說？」

「我想你應該也有聽說過了，沃爾特神殿正大肆宣傳大樹海裡有神敵。沃爾特神殿畢竟具有權威……基本上有很多貴族和維護治安的衛兵都是信徒，所以沃爾特的發言很有影響力呢～」

「唔……這恐怕很令人頭疼。」

儘管同情，但說實話，我無法與她產生共鳴。因為幸運草目前沒有神殿，況且這對我來說也不是迫在眉睫的問題。只要解決眼前的糧食問題，勇魔聯邦的糧食遲早也能夠自給自足，而且鹽巴和礦物資源也不是沒有門路可以取得。


應該說，鹽巴和礦物資源應該也可以經由開拓大樹海南部，有朝一日也能夠自給自足。大樹海南部有廣大的山脈，而如今也已得知山脈的另一頭有海洋。不論是開闢通往該處的道路、在山脈尋找礦山，還是前進大海，只要有我的力量，這些全部都有可能辦到。只差沒時間罷了。


話說回來，就算她告訴我她正為了國內的權力問題而煩惱，這也不關我的事。

「關於這件事，大概也只能靠你們自己想辦法了。」

「呵呵，是啊。」

見我聳肩，依露小姐微笑著這麼說。一旦情況危急，我只要從沒有阻礙的另一塊大陸，大量取得糧食就好。即使無法依靠密斯克洛尼亞王國和卡倫狄魯王國，我還是有辦法可行。

「只不過，唯獨有一件事我想請你們幫忙。」

「哎呀，是什麼呢？」

「我想你們應該也聽說瑪爾和美兒琪娜懷孕了，所以我想跟你們借用習慣照顧孕婦的人才。我家雖然也有人有經驗，但是……算不上一流。」

瑪爾一定有向依露小姐報告這件事……我才這麼心想，就見到他們兩人的反應怪怪的。依露小姐吃驚地瞪大雙眼，大叔則是用充血的眼睛瞪著我。喂，別這樣，很恐怖耶。

「……瑪爾沒有告訴你們嗎？因為害喜的症狀已經差不多快消失……所以應該已經懷孕超過三個月了。」

「我是第一次聽說這件事。那孩子真是的……不能把她帶到這裡來嗎？」

「沒辦法。因為轉移說不定會對腹中胎兒造成不良影響，而且儘管有鋪設街道，讓孕婦橫越大樹海移動到密斯克洛尼亞王國還是太危險。既然如此，還是在我們那邊創造一個可以放心的環境要來得安全、安心百倍。」

「只要你一路保護她，把她帶來就好了。要不然，我們也可以派人去接她。」

「我的意思就是，讓她遠距離移動這個主意太愚蠢了啦。要是馬車的震動和移動時造成的心理壓力影響肚子裡的孩子，那可怎麼辦？」

我拒絕了兩位試圖把瑪爾叫來自己身旁的意見。非關魔法，遠距離移動的風險太高了。

「我明白了，我會依你所願，安排一流的治癒魔法師。只不過，治癒魔法師大多是神殿的相關人士……」

「如果是鍛冶神巴魯甘德、酒神梅洛尼爾、風幸神賽菲爾、戰神迪奧的神殿相關人士，就沒有問題。因為這四位天神是我的盟友。」

「對喔。既然這樣，那就向賽菲爾神殿提出要求吧。」

「麻煩你們了。準備好之後，請利用通訊告知我，我會親自前來迎接。」

幸運草的領主館裡，有和依露小姐直通的通訊魔導具。我原以為瑪爾一定有利用那個報告懷孕的事情……為什麼瑪爾沒有告訴他們呢？

「大志，你目前的要求就只有這樣嗎？」

「是啊。因為俘虜所需的糧食已經在卡倫狄魯王國籌措好了……假如密斯克洛尼亞王國也有多餘的糧食願意賣我，我會很感激的。」

「知道了，我來安排吧。」

「我會以現金，或是我們那裡能夠取得的魔物素材付款。還有關於賊軍的事情，密斯克洛尼亞王國也有人參與嗎？」

「雖然好像有人對你不滿，不過我們這兒的貴族沒有人參與這件事。畢竟我們這裡怕你的人還比較多嘛。」

喔，這麼說來，我當初來密斯克洛尼亞王國做牛做馬地工作有成果了。

當時，我明里暗里全力支援依露小姐所施行的端正綱紀。不僅做過虧心事，也做過類似暗殺的事情。不過，我也不是她說什麼就做什麼啦。

基本上，我會先仔細調查過地方貴族和大商人，之後再使其失勢。但如果是太使人看不下去的傢伙，我就讓他被消失。現在回想起來，那些傢伙依舊令我作嘔。

「我在卡倫狄魯王國雖然盡了全力消滅魔物，卻幾乎沒有和貴族們打交道。可能因為如此，才會有現在的結果吧。」

總之，他們大概認為我是個只會殺死魔物的愚漢莽夫，打從心底瞧不起我吧。他們明明對我曾在密斯克洛尼亞王國大鬧過這檔事心知肚明，但畢竟不是親身經歷，所以沒什麼感覺。總有一天，我要讓那些傢伙懊悔不已。

「小心點，我們雖然沒有發現大舉利用身為貴族或大商人的力量行動的勢力，可是有沒有以個人為單位行動就不得而知了。密斯克洛尼亞王國畢竟是勇者之國……也有所謂的流浪勇者。」

「也就是說，那種人說不定會基於某種理由，單獨採取行動了？」

「正是如此～尤其神殿系的孩子們偶爾會做出失控的行為，所以很令人擔心……」

「麻煩你們管好自己國內那種棘手的傢伙啦……」

假如被稱為勇者的人才恣意妄為地四處大鬧，那就麻煩了。不過，我想依露小姐這樣的執政者不可能毫無對策就是了。

「因為王權無論如何都無法觸及神殿啊……如果是冒險者系的流浪勇者，就可以透過冒險者公會進行某種程度的控制。」

「我想姑且確認一下，應該不會有哪個國家將我認定為魔王，或是將我視為討伐對象吧？」

「是啊，這一點不會有錯。」

儘管如此，還是可能會有人來攻擊我啊……拜託饒了我吧。

「……總之，我會非常小心的。我可先聲明，由於狀況不容許我對欲取我性命者寬容以待，所以還請別見怪。」

假使懷有身孕的瑪爾和美兒琪娜有個萬一……我不想再想下去了。我突然好想見到大家，真想趕快回去。

「可以的話，我是希望你能以和平的方式解決問題，但既然攸關性命，那也沒辦法了～」

「正是如此。那麼，今天就到這裡……」

「嚴肅的話題就到此為止吧！」

「我們先告……呃……」

我本想說「我們先告辭了」，不料卻被依露小姐打斷。

「我雖然和妮菈見過幾次，可是我們也好久不見了，再說我和那兩位是初次見面，所以我想請你幫忙介紹一下，也想跟她們多聊幾句～」

見到她那麼遺憾的表情，實在很難在此告別……沒辦法了。

「呃，她是楠葉。是大樹海裡的阿爾嘉尼亞之鄉的首領，也是我的老婆。因為有擔任首領的經驗，而且基本上個性冷靜又很給我面子，所以我很信賴她。」

「臣妾名叫楠葉，請多指教。」

「然後這位是莉法娜。她是大森林的獵人，技術十分高明。我在大森林裡徘徊時，多虧有她幫助我。」

「請多指教。」

「我想你應該已經認識妮菈了……她真的幫了我很多忙呢。像是上位者該有的心態等等，她也教了我許多事。」

「請儘管依賴我。」

重新介紹完三人後，依露小姐便和她們和樂融融地聊了起來。嗯，男人好像沒法介入！不經意地望向大叔，他也一臉不知所措的表情。

我們對上眼，接著同時點點頭，從位子上站起來。

「那麼，我們稍微離開一下。」

「也好，我好久沒指導你了。」

「哈！到底是誰指導誰啊？」

「你們兩個不可以太勉強喔～」

我舉手回應依露小姐的提醒，之後便和大叔結伴從會客室走向王宮內的修練場。

「所以，瑪爾她還好嗎？」

「她很好，害喜的症狀也已經沒那麼嚴重了。因為還有一個人，就是精靈的美兒琪娜也懷孕了，所以她說有人陪著，心情上感覺輕鬆許多。而且我們那裡的居民也有人有生產經驗，所以沒什麼問題。」

「這樣啊。」

抵達王宮的修練場，保護王宮的衛兵和騎士們正在那裡揮汗訓練。一見到我和身為國王的岳父現身，正在進行訓練的士兵和騎士們立刻停下來，向我們敬禮。

「你們繼續吧。」

大叔舉手說完，所有人又開始訓練。

「他們雖然在宮裡工作，還是有確實進行訓練啊。」

「這是瑪莉亞的提議，就連宮裡的士兵和騎士，也得扛起定期掃蕩王都周邊魔物的責任。現在在這裡進行訓練的是內勤士兵，外勤士兵則正在周圍的村莊城鎮附近執行任務。」

「原來如此，這麼一來，在宮裡工作的士兵就不會鬆懈了。」

「是啊。這樣不僅可以為維護治安盡一份責任，而且只要依戰績發放津貼，士氣自然也會跟著提升。」

我們一邊交談，一邊各自物色訓練用的劍。由於劍的長度、重量五花八門，因此仔細斟酌自己方便使用的款式也是一種訓練。

「哼，我就知道會這樣。」

「就是啊。」

我和大叔選中的，是兩把一模一樣的劍。與其說是劍，那把寬幅看似非常堅固的劍其實更接近鈍器，一般士兵恐怕就連揮舞這把劍都很困難。說起來，這把劍的用途並不是練習互擊，而是用來空揮以培養肌耐力。

我移動到沒人使用的比試用訓練區，空揮了好幾次劍。大叔則是脫掉奢華的斗篷，接著又把上衣脫了，一身輕便。

「那麼，要稍稍比劃一下嗎？」

「好啊，就來比劃一下吧。」

我們帶著笑意，互相對峙。兩人相隔只要彼此再往前踏一步，就會進入攻擊範圍的距離。

我徐徐沉下腰，蓄積往前踏出的彈力。我們雙方都側著身體，將沒有持劍的左肩往前推，不讓對方看見自己的劍鋒。也就是說，我們自然而然地都擺出相同的架式。

旁觀我倆對峙的某個士兵，發出吞咽口水的聲音。

「吁！」

「喝！」

我出招那瞬間的速度非常快。由下而上斜向挑起的一擊被大叔的劍打落，接著大叔利用反作用力往上一躍，將劍朝我的頸子砍來。

我退後半步閃避刺向頸子的一擊，並且搪掉追擊的突刺，之後這一次輪到我使出突刺反擊。大叔也側著身體後退，避開突刺，但是我稍微感覺到這一擊生效了。仔細一瞧，大叔的衣服前襟微微裂開。

「你變厲害了嘛。」

「被迫身陷困境當然會成長了。」

「原來如此。既然這樣，就讓我見識一下你所謂的成長吧。」

對話到此為止，之後我們沉默無語地彼此交鋒。被注入最低限度的魔力的練習用劍每次相觸都激起火花，劃破空氣的劍所產生的劍壓撕裂修練場的地面。

大概打了一分鐘吧？大叔的動作開始變得遲緩。似乎是VIT值的差異讓耐力產生了差距。我雖然還能打，但對方似乎撐不下去了。

我利用大叔向我釋放的衝擊力道，大幅拉開間距，重新掌控局面。

「唔，我雖然覺得自己成長了，卻還是無法進攻。」

「哈、哈、呼……！你真的變厲害了。」

一面心不在焉地聽著大叔的評論，我觀察自己的劍的狀態。唔，損傷得相當嚴重耶。反觀大叔的劍則是還很完好。

「不，看來我的能力還是不足。如果你我條件相當，我的劍恐怕會先壞掉。」

「也許吧。不過，我也可能在那之前就會被打倒了。」

大叔帶著淺笑這麼說。事實上，只要沒有突如其來的奇襲，我應該是不會輸給大叔。因為，如果是像這次一樣使用鐵劍進行的比試就算了，若是使用神銀或山銅制武器進行的實戰，想必不會發生武器遭到破壞的事態。

「還要繼續嗎？」

「可以啊，就讓我來好好地指導你。」

「哈哈，我一定要把你打趴在地。」

「那你就試試看吧！咿呀啊啊啊啊啊啊啊！」

就在我弄壞四把、大叔弄壞兩把練習用劍之後，負責管理裝備的士兵哭著阻止我們，於是我們結束這場比試練習。

我順著大叔的建議去衝個身體後，回到了剛才會談的會客室，結果四位女性還在和樂融融地聊天。哈哈哈，這些女人。

我以太陽即將西沉為由，請依露小姐放了三個老婆，四人一同返回幸運草。

「依露歐妮殿下真是一位了不起的人物。」

「就是啊，真想再跟她多聊聊。」

「我好希望有一天能夠成為像她那樣的女性喔。」

然後三個老婆就這麼被依露小姐拉攏了。依露小姐真可怕……我是說真的。
