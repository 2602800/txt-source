「我的房間裡還什麼都沒有哦，這樣也可以嗎？」
「沒關係，不如說是這邊想拜託你。我的房間現在⋯⋯稍微有點亂啊。如果可能的話，想在稍微收拾整齊後再招待你過去」
「明白了」

二人這麼說著朝樓下走去。目標是在這一樓層前方的麻里子的房間。

「誒抖，是這裡吧，我的房間」

在一扇門前站住的麻里子向米蘭達詢問。由於有很多同樣構造的門排列在一起的緣故，現在對自己房間的位置不是很有自信。

「啊。左邊相鄰的是我的房間，所以是這裡沒錯」

（雖然聽說了就在旁邊，壁櫥的對面就是米蘭達桑的房間嗎）

「非常感謝。那麼請，米蘭達桑」
「失禮了」

麻里子用房間鑰匙打開門招待米蘭達進去。雖說這是麻里子的房間，但其實僅僅只是睡了一晚上而已，這個房間裡能稱得上是私人物品的也只有上面空空如也的桌子、椅子以及床了。

「啊，麻里子桑。順便說一下。那個窗外是中庭。有的是，水井、晾衣處和庫房這樣的東西」

聽聞後，麻里子拉開拉窗看了看外面。中庭對面的牆上有著一扇門，院內的東西正如米蘭達所言。之前在洗滌場遇到過伊莉，中庭已經收完了是這麼說的，因此現在在晾衣架上並沒有看到什麼在晾晒的衣服。

「請坐下吧。米蘭達桑」
「啊，啊啊。坐在床可以嗎？」
「是的，沒關係呦。請」

米蘭達在床角邊孤零零的坐下。尾巴似乎是轉向身體的一側，從腳旁邊伸了出來。

「那麼麻里子桑，來完成約定吧。從哪邊開始比較好？　如果是想要尾巴的話，雖說是有些失禮，但我認為我這邊躺下會比較容易觸摸到」

米蘭達以不經意的態度說著。耳朵噼庫哩地擺動著，從裙的下擺中探出的尾尖也在不停地搖晃。麻里子壓制住想要衝過去進行揉搓的雙手，悄悄地咽了口唾沫。

「那，那麼，先從耳朵」
「知道了」

麻里子急忙解開了長筒靴的帶子將其脫下並換上涼鞋，來到米蘭達旁邊彎下腰。首先從近距離開始觀察起耳朵。那稍稍剪短的頭髮之間三角形的耳朵從中跳出。在耳朵外側生長的毛髮並不像頭髮那樣長，是很短但整齊統一的生長著，同時在那上面描繪有虎皮樣的花紋。在耳朵內側一直到一定深度上生長著柔軟蓬鬆的白色毛髮。再往深看去就是如同人類耳朵一般是長有類似於胎毛的皮膚。

「真的與貓耳相似呢。唔唔，好可愛⋯⋯」
「是嗎？至今為止，從來沒有被那樣說過」
「不不，很可愛的呦，絶對的呦，肯定沒有錯的呦」
「是，是那樣啊⋯⋯」

被稱讚的米蘭達的臉上浮現出些許赤紅。

「話說回來米蘭達桑，這個頭髮上的條紋是真的嗎？」

在展開頭髮的現在，可以發現米蘭達的頭髮是由那橙色頭髮之中摻雜有茶色的頭髮來生長的。

「啊啊，基本上在耳朵和尾巴上都有著同樣的條紋。一直都是那樣雜亂生長的程度。啊，難道是想看條紋，這樣的話⋯⋯」
「不是哦，那種事。十分的漂亮不是嗎」
「是那樣嗎？」
「是的」

麻里子如此斷言。

「那麼，能讓我摸一摸麼？」
「嗯？　啊啊」

米蘭達將耳朵伸了過去，朝向麻里子的方向將腦袋稍稍傾斜。她的眼睛以很自然的樣子仰視起了麻里子的臉龐。

（庫，貓耳美少女的仰視！！）

麻里子壓抑住差點就要發出來的聲音，向米蘭達的耳朵伸出手。從耳根部開始輕輕地撫摸。

「呀！」

被撫摩的瞬間，那與自己觸摸時所不能及的酥癢感讓米蘭達不由得的閉上眼睛叫了出來。這使得麻里子驚慌的鬆開手。

「啊，對不起。痛嗎？」
「不，沒什麼。只是稍微有些癢。大丈夫，繼續下去吧」
「要是受不了的話要告訴我」
「啊啊」

麻里子再次開始輕柔的撫摸起貓耳。

（噢噢，貓耳啊）

「恩」

為了好好看清毛髮而將其捋順。

（多麼柔軟的毛啊）

「嗯嗯」

耳根部用指甲去撓的話有著咯吱咯吱的感覺。

（根部的形狀也和貓咪一樣啊）

「Fua」

輕輕地捏住耳垂撫摸上去。

（好大，耳垂兒也要厚一點。噢，好柔軟）

「Hiu」

往耳朵內側窺去，在那裡長有順滑的毛兒。

（內側的毛要更加柔軟啊）

「呀啊！等，等，麻里子大人。姆，耳，耳朵。呼，呼吸。啊」

盡可能的壓抑聲音不讓它發出來的米蘭達，終於忍受不了身體顫抖的發出了聲音。那因脫力而傾倒下去的身體被麻里子急忙扶住。

「不要緊麼，米蘭達桑」
「大，大丈夫⋯⋯只是，有些，太激動⋯⋯」

身體被扶在懷中，米蘭達以濕潤的雙眼仰望著麻里子。