「歡迎回來。」
「啊啊。」
「阿！希拉小姐。已經回來了阿。」
「好久不見阿，小艾達。」
「嗚齁齁。」
「傑利可也好久不見了。來，伴手禮的水果乾拼盤喔。」
「嗚齁齁──」

這麼說來，雷肯回想著希拉以希拉的姿態和艾達見面是自何時以來，但想不起來。

「茶正好剛泡好了。喝吧。」
「希拉小姐的茶很好喝，最喜歡了。」
「艾達。」
「嗯？」
「你，說話方式變了嗎？」
「嗯。因為不用勉強也沒關係了。」
「勉強？」
「勉強讓說話方式像個冒険者會比較好，之前是這麼想的。」

（原來那個奇妙的說話方式，是特意的嗎）
（搞不懂為什麼會覺得那像冒険者就是了）

希拉的茶很美味。
是能消除深刻疲勞的味道。

「很順利的樣子呢。」
「希拉小姐知道嗎。」

雷肯和艾達從神殿直接走了過來。
但希拉似乎已經知道神殿發生的事了。

「不過呢。不會強制這話，還真狡猾呢。」
「對阿。副神殿長沒有說，不會再勸誘呢。」

艾達如此說道，讓雷肯感到有點感動。
有注意到這一點，代表有好好理解那你來我往。

「雷肯，做得好。辛苦了。」
「啊啊。」
「但是，最後破壊了佐格斯神像很糟糕喔。」
「啊啊。」
「結果不得不做無償奉仕。還九次呢。」
「啊啊。」
「你是笨蛋嗎。」
「⋯⋯」
「希拉小姐，孤兒院的工作是無償的嗎？」
「這次的場合估計是吧。畢竟不是工作而是奉仕。雷肯在神殿的孤兒院無償奉仕，代表他尊重神殿。」
「這樣阿。話說回來，妮姫小姐呢？」
「我拜託了妮姫一些事，會離開城鎮一陣子。」
「希拉。我有問題。」
「什麼問題。」
「我跟艾達有必要去神殿嗎？只要告訴神殿，我是迷宮踏破者，以及我跟艾達離開的話，妮姫和希拉也會跟著離開，不就行了嗎？」
「雷肯。」
「嗯？怎麼了，艾達。」
「妮姫小姐有說，如果我跟你得離開的話，就會一起離開。但是，希拉小姐沒有說過喔。副神殿長雖然這麼說了，但是我們甚至沒告訴過希拉小姐這件事。」
「你阿。」
「嗯？」
「腦袋突然變好了阿。」
「小艾達。那是因為我從妮姫那得知了事情，然後傳達給副神殿長。雷肯，那樣可不行喔。」
「什麼不行。」
「那樣的話，卡西斯神官就不會收手。會被禁止勸誘自己盯上的〈回復〉持有者。這種場合，就會探查你跟小艾達的弱點，一點一滴地進攻吧。你很討厭也不擅長，對付這種作法吧。」
「確實討厭也不擅長。」
「這種類型阿，會在暗處搞事，太煩人了。所以就直截了當地惹怒他，等露出破綻後再擊潰是最好的。」
「是覺得我去的話，那傢伙就會發怒並露出破綻嗎。」
「那也是當然的吧。你根本不把神官的權威當一回事，而卡西斯神官那種聖職者，不會放過不尊敬自己的人。打心底認為冒険者什麼的就是垃圾喔。所以最初擺出了完全看不起你的態度對吧？」
「然後就跟副神殿長商量了嗎。」
「那個老太婆阿，明明很有能力卻很怕麻煩吶。不過，這次是把髒老鼠關起來的絶佳機會，所以就協助了。說是這麼說，卡西斯神官雖然身負惡評，但對神殿的貢獻度很高，也有很多人脈。善後會很辛苦吧。」
「希拉。」
「怎麼了？」
「知覺系魔法有個叫〈遠耳〉的對吧。」
「有阿。」
「能聽到多遠的聲音和對話？」
「通常只能停到隔壁房間的對話。不過沙漠這種沒有人的場所，有時也能聽到幾千步遠的聲音就是了。」
「歷史上最優秀的魔法使的話，就能從這裡聽到神殿的對話吧。」
「如果那個魔法使有正確把握神殿的位置和形狀的話，說不定可以吧。」
「原來如此。話說，卡西斯神官會被驅逐嗎。」
「為什麼得做那種危險的事阿。可不能放任那種人喔。」
「也就是說，會被神殿養到死嗎。」
「應該吧。」
「對了。有件事想問。」
「嘿？」
「我同時擊出了好幾十支〈火矢〉。」
「有看到喔。」
「什麼？」
「那種荒唐的東西還真是久違了。你是想擊落星星嗎？」
「但是，不知道該如何好好擊發。沒辦法想像得很明確。我想學那個的射擊方法。」
「嗯─。那個嗎。嘛，也行。會在最近教你的。比起這個，該談的是你們的今後。」