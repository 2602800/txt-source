雷肯在心中嘆了一大口氣。

在過來的途中，考量著該讓對話怎麼進展。
當時注意到了，不能讓艾達施展〈回復〉。

艾達的〈回復〉有可能包含著〈淨化〉。這神殿目前雖然沒有〈淨化〉持有者的樣子，但仍舊是神殿。神官們是這方面的專家。讓艾達的〈回復〉被看到就已經很糟了，要是被發現有著〈淨化〉的才能的話，就完了。艾達會失去平安，就算離開了這城鎮，也永遠不能再回來了吧。

儘管如此，故意失敗也很危險。
首先，就算想要失敗，也可能會不小心成功。此外，就算順利地失敗了，神官們也可能從魔力的運作，察覺到真相。

所以，雷肯要自己來施展〈回復〉。

「那麼，現在施展〈回復〉給你們看。會在這右手上顯現。仔細看好了。」
「什麼？杖呢？」

雷肯張大右手伸向前方，並讓手掌朝上。
長長的五隻手指，張得很開。

（拜託了）
（讓我成功吧）

事實上，在前往邦塔羅伊的旅途中為凡達姆施術那次，是雷肯最初，也是最後一次，實際以〈回復〉治療人。之後也有持續練習，在艾達覺醒了高次元的〈回復〉後，也作為實驗台，親眼看過好幾次那情況。但是，就這樣而已。

（拜託了）
（如果我失敗的話）
（或是只能做到初級的〈回復〉的話）
（我說的話就會沒有說服力）
（會讓矛頭指向艾達）
（那就很糟糕）
（所以為了艾達）
（為了保護艾達）
（拜託了！）

「〈回復〉。」

寧靜地，毅然地說出發動咒文後，雷肯的右手上，顯現出了綠色的光球。

「喔喔！」
「這實在是。」
「多麼美麗阿。」
「多麼的巨大。」
「這，這是。」

雷肯自己也很驚訝。
這〈回復〉是如此巨大、明確又強力。

（謝謝了，神阿）

雖然不知道是什麼神，但雷肯向幫助了自己的神獻上了感謝。

說到底，雷肯雖然討厭神殿，但相信著神。應該說，對賭上性命生存的冒険者來說，會時不時經驗到，只能向神祈禱的瞬間。此外，向神祈禱並施展不得了的招數的神官和騎士，也見過太多太多了。對神明的敬畏，深根於雷肯的心底之中。

但是，這件事，和把艾達交給來歷不明的神官們，完全扯不上關係。而且在來到神殿，見到卡西斯神官後，又再次認為，絶對不能把艾達丟在這裡。

不知何時，神官們的騷動也靜了下來。
雷肯右手一揮，綠色燐光便消失了。

「喂。剛剛的〈回復〉。」
「沒有，準備詠唱⋯⋯」
「怎麼可能！」
「確實是，只有發動咒文而已。」

卡西斯神官張大的眼裡殘存著驚訝，浮現出並非憤怒或憎恨的感情，顏面痙攣著。

「卡西斯神官。」
「怎樣，雷肯。」
「剛才的是，我貨真價實的，全力的〈回復〉。」
「確實是〈回復〉。」
「依照凱雷斯神殿的基準，剛才的是初級？還是中級？」

卡西斯神官雖然閉著口，但帕吉爾神官回答了。

「很明顯是中級以上。」

其他的神官也補上話。

「而且，那個光球的大小。有著不得了的回復量吧。」
「如果是那個，相信能在一瞬間治療瀕死傷患的重傷。」

雷肯嘆了一大口氣。這下總算越過山頭了。

「我的弟子艾達，雖然沒到剛剛這種程度，但能做到這一部分的〈回復〉。」
「喔喔！」
「太美妙了。」
「該讚美神之偉業阿。」
「正如你們看到的，我的流派沒有準備詠唱。」
「就，就是那個！」
「到底要怎麼做，才能辦到。」
「沒，沒錯。先不論偶然發動的弱小〈回復〉，剛剛的那種〈回復〉，是不可能省略準備詠唱來發動的。」
「也就是說，凱雷斯神殿，沒有省略準備詠唱來行使〈回復〉的方式對吧。」

這失禮的質問，讓神官們憮然地沉默了。
開了口的是，帕吉爾神官。

「準備詠唱能提升術的精度，強化效果。本神殿重視仔細又正確的準備詠唱。」
「神殿的話，是那樣吧。但是，迷宮不同。」
「你說，迷宮？」
「沒錯。迷宮會在一瞬間決定生死。所以我所有的魔法，都沒有準備詠唱。」

卡西斯神官插了嘴。

「你說，所有的魔法？雷肯，你是魔法使嗎？」

進入神殿時不能帶劍，就把劍放進〈收納〉了。不過，看了雷肯的身軀後，沒有人會認為是魔法使。

「本業是劍士。但是也能用魔法。」

長久的沉默又再次降下。神官們因為遭遇了不符合自己常識的事而混亂著。現在是撤收的好時機。

「剛剛，我證明了能使用〈回復〉。艾達身為我的弟子，雖然〈回復〉和其他魔法都還不成熟，但也在成長。總有一天也能做到跟剛剛相同的〈回復〉吧。這就是你們的疑問的解答。我們能回去了嗎？」