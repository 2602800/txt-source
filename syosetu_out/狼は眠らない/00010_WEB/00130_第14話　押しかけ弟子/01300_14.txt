雷肯來到了前庭。

然後從〈收納〉取出了〈拉斯庫之劍〉。
周圍被薄暮包覆，夜幕正要籠罩城鎮。

阿利歐斯遲了些才出來。拿著短劍。是從身上的〈箱〉拿出來的吧。
艾達追了出來。

「艾達。要看就到庭院旁邊。在那裡待好。」
「是─。」

雖然是個狹小的庭院，但當作是迷宮裡的戰鬥就剛剛好。
雷肯今天，沒有穿〈貴王熊〉的外套。
穿著沒有防禦力的衣服，站在阿利歐斯這等劍士面前，感覺到了刺激的緊張感。這緊張感，雷肯並不討厭。
阿利歐斯以正手握著短劍。

「阿利歐斯。你的短劍沒有反握的招式嗎。」
「反握，嗎。雷肯殿是想讓艾達殿學習守勢之型是嗎？」
「手勢？」
「防守，的意思。」
「沒錯。」
「那請安心吧。我的流派也有正握的守勢之型。是也適合在狹窄場所戰鬥的型。」
「那就好。」

兩人閉上了口。
既然是長劍與短劍的戰鬥，那長劍先出招才算得上是禮儀吧，雷肯這麼想著，便踏了出去。
此時阿利歐斯衝了過來。

（喔）

沒有預備動作的快速衝刺。果然這男人很行。
將握在右手上的短劍拉到左方，阿利歐斯意圖筆直鑽入雷肯懷中。
雷肯以迎擊的姿勢，從上方往下揮劍。

（糟糕！）

要是就這樣把劍揮下的話，阿利歐斯會在擋下雷肯的劍的同時，鑽進懷裡。被誘導為這種姿勢了。
雷肯馬上彎曲了雙膝。要是阿利歐斯就這樣衝進來的話，會因為體格差被雷肯壓倒。
對此，阿利歐斯迴轉身軀，移到雷肯的左側。

（很行阿）

雷肯毫不焦慮，順著彎曲膝蓋的動作往右前方跳去。
驚人的是，阿利歐斯緊跟著雷肯的移動，也移動了過來。有著很棒的反射神經及肌肉柔軟度。
阿利歐斯以短劍突擊雷肯的左側腹。雷肯轉身以劍彈開短劍。
然而，短劍的角度比雷肯預想得還深，雷肯的劍偏到了右側。

（這招漂亮）

向著感到欽佩的雷肯那沒防備的腹部，短劍順到像是被吸了過去。但是，預料到此行動的雷肯，以左手緊緊握住並止住了阿利歐斯的右手。那右手上握著短劍。
兩人的動作都突然停止。

「阿利歐斯君，加油─」

（喂。應援的對象錯了吧）

短劍的刀背，添附著彎曲的左腕。這把短劍是單刃。這左腕的輔助，就是能不輸雷肯的劍的力量的原因。
想再看一下阿利歐斯會怎麼戰鬥，因此，雷肯在左腕使力，把阿利歐斯推出去。
被推飛的阿利歐斯，腳底與地面磨擦，退到了後方。但是沒有失去平衡。
雷肯轉過身子，背對阿利歐斯。

「就是現在─。上吧─」

（妳是站在哪邊阿）

在劍與劍的戰鬥中，距離感是最為重要的。但是，與背對的對手的距離感不好抓。在這種狀態下，阿利歐斯會展現什麼樣的戰法呢。

消失了。
阿利歐斯的氣息消失了。

（這還真是驚人）

當然，雷肯的〈立體知覺〉，有清楚地映照出阿利歐斯的身姿。但是，如果雷肯沒有這特殊能力的話，就沒辦法察覺阿利歐斯的位置和動作了吧。

這是某種技能嗎。
思考時，阿利歐斯仍在逼近。

從防守的狀態反擊時，阿利歐斯展現的動作是那麼的變幻自如，但由自己發動攻擊時，動作有些單調。而且太乾淨了。
雷肯看也不看後方，將右手的劍投擲到後方去。

毫無偏離準心，劍直直迫近阿利歐斯的頭。完全處於攻擊狀態的阿利歐斯，反應遲了一點，在千鈞一髮之際，用短劍彈開了飛來的劍。
但姿勢在此時被打亂，沒辦法即刻對應迅速轉身的雷肯。

雷肯再次，以左手緊緊握住阿利歐斯握著短劍的右手。而且是以隨時都能讓右手攻擊的姿勢。雷肯緊握連岩石也能粉碎的拳頭，阿利歐斯的肌肉顯露了緊張。

「好。到此為止。」

雷肯放開手後，阿利歐斯單膝跪地，低下了頭。

「我甘拜下風。」
「啊啊。」
「實在誠惶誠恐。想不到，竟然連〈隱足〉，也被輕易破解了。」
「引足？剛剛消去氣息的招式嗎？」
「是的。是我的流派傳承的步法。」

也就是說，這招式是有辦法教給別人的。

這一瞬間，雷肯捨去了在這四五天把阿利歐斯用完就丟的計畫。
就算做不到那麼完美的氣息隱藏，只要多少能學會不容易被敵人注意到接近和移動的招式，這就會成為艾達無可比擬的財產。

而且，到今天為止都不知道，短劍原來是如此便利的武器。
此外，也清楚了解到，阿利歐斯很適合作艾達的短劍師傅。

使用匕首或刀子戰鬥，就不得不鑽入對手的懷裡。使用劍和短劍的初心者，經常會想攻擊揮劍距離外的敵人，然後被避開或失去平衡，露出致命的破綻。武器這種東西，如果不在適切的攻擊距離內使用，便發揮不了威力。而短劍，是應當在對手懷裡使用的武器。僅僅如此就需要勇氣。

這一方面，阿利歐斯並非拉開距離，而是在非常近的距離內使用短劍。如果能學會那呼吸與動作的話，艾達的防禦就會變得很堅實吧。
擔心的只有一點。

「阿利歐斯。單刃的短劍，是一般的店裡會賣的東西嗎？」
「應該沒有賣吧。直接委託鍛造師製造，會比到處找還快吧。不過，只要在左腕上穿戴護腕，就也能以雙刃的短劍使出剛才的防禦招式。」
「是嗎。好。就教艾達短劍的招式吧。」
「好的。合格了嗎？」
「合格了。另外拜師也合格了。說是這麼說，我也不知道什麼戰鬥方式的教法。就觀察我的戰鬥方式，自己學吧。」
「是。非常感謝。」
「剛才說的四五天就取消了。有了這四五天和另外六天，就能完成我目前要辦的事。不過也要看對方的情況，會誤差個幾天也說不定。」
「好的。在這期間裡，教艾達小姐短劍的基本防禦技就可以了嗎？」
「對。在那之後，我跟艾達會去尼納耶迷宮。你有去過尼納耶迷宮嗎？」
「不。沒有進入過迷宮。」
「是嗎。你也來吧。」
「好的。」
「來一起探索迷宮，在那裡偷我的招式吧。」
「好的。」
「另外，能教艾達消除氣息的招式嗎？」
「要學那個，艾達小姐就必須要有那方面的資質才行。」
「那麼，就幫她確認有沒有那個資質吧。之後的就等確認完再說。」
「好的。」
「雷肯。」
「嗯？」
「那六天，是要去孤兒院對吧？」
「沒錯。」
「誒？是要去哪裡？」
「有恐怖的對手在等待的場所。」
「竟然能讓雷肯殿說到這種地步，究竟是何等敵人？」
「說不定，是我至今以來遭遇過的最難纏的對手。原本是打算拖延的，就一口氣決勝負吧。」

艾達雖然說孩子們很喜歡雷肯，但雷肯果然還是不擅長應付孩子們。如果把孤兒院之行放著，感覺就會沒辦法打心底享受迷宮。

把討厭的事情先解決掉，抱著暢快的心情去迷宮。
雷肯如此決斷了。