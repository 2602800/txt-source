﻿
為了明天做好準備，誠一他們開始休息的時候，【封魔之森】裡凱澤爾帝國同樣佔據著陣地。
這些凱澤爾帝國的士兵們，一邊做著各自的野營準備，一邊在敵國附近拿出酒，完全沒有警戒的樣子就像宴會一樣開始吃飯了。

「隊長，那些傢伙真難搞啊。」
「哼，沒錯。」

在這次瓦爾夏帝國遠征中擔任指揮的是凱澤爾帝國第一部隊隊長的奧利烏斯芬薩。
與第二部隊不同，由貴族構成的第一部隊，無論是誰都是『超越者』。

「但是......因為紮基亞那混帳派不上用場，我們才來到這樣偏僻的地方。」
「真的......所以說平民真討厭。盡是無能之輩，只會拍馬屁。」
「那些傢伙只要默默地交錢給我們就好了。」

奧利烏斯率領的第一部隊的人，只是把紮基亞他們和凱澤爾帝國的國民當作提款機。

「這麼說來，隊長。今晚不採取攻擊嗎？」
「啊？是啊太麻煩，算了。」
「太麻煩嗎......。」

奧利烏斯一邊喝酒一邊回答部下的話。

「我沒說錯吧？我們用陛下的力量成為了『超越者』。只要拿出真本事隨時都能擊潰他們。」
「喂喂，你在擔心什麼？看看這傢伙。」

奧利烏斯站起來，抓住了附近的樹

「哼！」

然後，奧利烏斯用力的瞬間，手指貫穿堅硬的生木，就這樣拔出紮根在地上的樹木。

「看吧，這股力量！我們所有人都可以做至今為止沒有做過的事情。不走運的是，這個地方不能使用魔法。但是那又怎麼樣？我們有這股力量。哪裡有擔心的要素呢？而且......看！」

奧利烏斯一隻手拿著樹木，在離士兵們稍微遠一點的位置踩了一下，地面大幅下沉，形成了凹洞。

「啊哈哈哈哈！好厲害，這股力量！只要有這力量，就不會輸！不是嗎！？」
「是啊......我錯了。」
「哼，明白就好。」

奧利烏斯隨意地將手中的樹木扔掉，露出了噁心的笑容。

「而且，那些傢伙已經無能為力了。連找其他國家幫助的事都做不到啊？不管怎麼說，幾乎所有的國家都被我們凱澤爾帝國所支配。從現在開始......讓我們慢慢地享受一下那些傢伙的悲鳴吧。」

聽了奧利烏斯的話，其他的士兵們也浮現出陰險的笑容。
這時，一個士兵好像想到什麼的開口說。

「對了，隊長。攻陷了瓦爾夏帝國之後，我們有獎勵嗎？」
「不用擔心。赫里奧大人說了，如果攻陷這個國家，我們可以隨便處置那裡的平民。盡情地玩耍吧！」
「呀呼！不愧是【幻魔】大人！」
「這真是不努力不行呢！」

浮現出下流的笑容，他們各自開始妄想侵洛瓦爾夏帝國之後的事。

「喀喀喀，真令人期待啊？而且，聽說瓦爾夏帝國的【女帝】是個大美人，侍奉在她身邊的漂亮女人也很多。我們一定要好好享受呢？」
「隊長！這樣好嗎？我們先收下那個女帝......赫裡奧大人不是說，只有平民可以隨我們處置嗎？」
「混帳東西。那是因為陛下和赫裡奧大人想要貴族的子女和女帝吧。正因為如此，我們快活完後就殺掉她們。沒什麼，只要對陛下和赫裡奧大人說她們是自殺的就行了。遇到這種情況即使她們會自殺也不奇怪。只要殺掉她們就不會追究我們，都來到這麼偏僻的地方，就算玩一下身份高貴的女人也不會受到懲罰的。」

喝著酒，醉意襲來的奧利烏斯坦然地說道。
如果其他身份高貴的人聽到這句話，就算是奧利烏斯也不會得到饒恕吧。
但是，作為由與奧利烏斯相似的思考回路的人組成的第一部隊，大家都喝醉了，並且在無法抑制住欲望的現在，誰也沒有被責備。
豈止如此，大家都只想著如何侮辱身處瓦爾夏帝國的女帝及其親信們。

「哎呀......可以堅持到什麼時候呢？說不定馬上就會投降了？」

在不久的將來，奧利烏斯一邊幻視著跪求原諒的女皇的姿態，一邊舔著舌頭。
儘管他們正在戰爭中，但他們已經一邊喝酒一邊對自己的勝利毫不懷疑。
他們根本沒想過這個夜晚會有來自瓦爾夏帝國的襲擊。
實際上，瓦爾夏帝國方面也沒有對凱澤爾帝國進行夜襲的人手和餘裕，奧利烏斯他們的想法是正確的，但就算是這樣，他們也沒有太大的警戒心。

「我們是狩獵的一方。慢慢地，一邊享受著對方的痛苦一邊前進吧。」

──正因為如此，他們才被至今為止從未親身體驗過的『不講理』所襲擊。
即使在這個夜晚對威爾夏帝國發動了攻擊，他們的未來也不會改變。

◆◇◆

「哼......一群雜魚。看起來近期就會死吧。」

正當利烏斯他們喝著酒，完全放鬆的時候，從稍微遠的位置戴著帽子的人靜靜地注視著他們。

「真是的......還差一點點就讓瓦爾夏帝國陷入混亂了，那個女帝......。」

戴帽子的人不耐煩地那樣嘟嚷著，瞪視著瓦爾夏帝國。

「算了。雖然瓦爾夏帝國內的任務失敗了，但現在讓我利用一下這場戰爭吧。如果瓦爾夏帝國就這樣落入了凱澤爾帝國的手中，想必有很多高品質的『負的感情』會聚集在一起吧。」

這樣笑著的戴帽子的人手背上，刻著作為【魔神教團】的『使徒』證明的徽章。

「凱澤爾帝國的士兵都是傻瓜，所以沒有注意到我的存在，但瓦爾夏帝國卻發現並且警戒著我。但是，我絕對不會現身的。即使不露面，只要在背後強化這裡的魔物和凱澤爾帝國的士兵們就行了，盡情在意識到我的情況下戰鬥吧。」

戴帽子的人是莉艾爾和她們警戒的，現在對戰爭橫加干涉的『使徒』，不過，莉艾爾她們無法捕捉他的身影，不僅僅是凱澤爾帝國，這個『使徒』也需要多加注意。

「不過，如果注意力從我身上移開，那一瞬間就會被殺掉的。」

正如戴帽子的人所說，如果注意力被分散了，即使是一殘間，如果被這個「使徒」看到破綻的話，只能預測到被殺的可能性。
他一邊眺望著胡鬧的凱澤爾帝國士兵，一邊為了讓自己休息而爬上樹坐下，他突然想到。

「這麼說來聽說迪斯特拉大人也會來這個地方，現在怎麼了？如果那位大人來的話，就沒有必要像現在這樣躲起來行動了......。」
知道作為【魔神教團】最強戰力的迪斯特拉會過來的戴帽子的人，為迪斯特拉到現在還不來的事絞盡腦汁。

「......不過那位本來就很忙。一定是接受了其他的『神徒』大人或者魔神先生親自下達的委託吧。話雖如此，他早晚會來的，在那之前，讓我好好攪亂這個地方吧。」

雖然迪斯特拉已經不會踏上這片土地了，但是不知道這些的戴帽子的人做出了結論，就去休息了。
──然後，他也體會到那莫名其妙的『不講理』。

◆◇◆

「──那麼，要去試試看嗎？」

第二天。
一大早我就從正門走出去，一邊做著輕微的準備運動，一邊拔出【憎惡滿溢的細劍】。
士兵們也開始各自準備，我一個人走出正門外，看門的士兵們驚訝地看著我。也對啦，對亞美利亞她們一句話都沒說就出來了。

「對了，昨天樹說了接下來還有事，它想讓我做什麼呢？」

原本是因為樹的委託才來幫忙的，昨天是用了回復藥和回復魔法等後方支援的方法......難道還有我不知道的問題嗎？算了，等這個結束後再問吧？

「首先......凱澤爾帝國的士兵在哪裡......呢......！」

我在原地輕輕地跳上去，一瞬間到達了可以縱覽【封魔之森】的位置。
我試著從空中尋找凱澤爾帝國士兵的位置，就在離這裡不遠的地方，發現了與瓦爾夏帝國的士兵穿著不同鎧甲的士兵集團。那就是凱澤爾帝國的士兵吧。
為了確認有沒有看漏其它地方，我發動了『世界眼』，在凱澤爾帝國士兵們聚集的地方附近，似乎只有一個人有別的反應。
通過對那傢伙用『世界眼』來確認，終於知道他是【魔神教團】的『使徒』了。
幸好那傢伙和凱澤爾帝國的士兵們的位置很近，這次我想做的事情可以把他們一起解決。
在確認了那個之後，我從飛上去的位置自然降下來，突然看到門衛們用呆呆的表情看著在高空的我。

『......』

大家都目瞪口呆想要說點什麼似的。說實話，如果這就被嚇到了，現在就要開始做的事情會有什麼反應呢。
先不管這個了――――。

「馬上就結束了！」

在自然降落的過程中，我重新握住手中的黑劍，一度以空中為立足點，一口氣跳到了凱澤爾帝國士兵的背後。
雖然是因為一瞬間移動到背後，但多虧士兵們身處鬱鬱蔥蔥的森林中，由於樹木的妨礙而看不到我身在高空的身影。
我確認了之後，從在【封魔之森】構築陣地的凱澤爾帝國的士兵們那裡，以稍微偏離的位置為目標揮動了黑劍。
然後，黑劍出現了超巨大的斬擊，在凱澤爾帝國陣地的後面做出了巨大的溝槽或者說是斷層，就這樣變成了一個邊界線。順便一提，那個斬擊是稍微斜著進入地面。
不愧是注意到斬擊的規模和衝擊的大小，凱澤爾帝國的陣地開始騷動起來。
但是，發出斬擊是我......不，好像因為沒有想過斬擊會從上空飛來，所有人都開始尋找錯誤的方向。
如果就這樣各自開始移動的話，會成為我考慮的方法的障礙，我繼續進行工作。

「哼！哈！哈！」

用最初放出斬擊的要領，為了讓凱澤爾帝國和隱藏起來的【魔神教團】的傢伙不會逃走，我用斬擊痕做出一個正方形。而且每個都稍微斜著切入，把斬擊打進地下。
那個正方形有相當大的切口，我就這樣回到地面，眼前是成為正方形一邊的斬擊痕，我將手插入那裡。

「那麼，真的能做到嗎？」

連自己也半信半疑──我一邊想一邊把那個地面舉起來了。

「喂，真的假的？」

雖然是自己想做的事，但沒想到真能做到啊。
──我想做的事。就是把【封魔之森】的一部分切下，就那樣搬運到其它地方，一般來說是不會考慮的，不能稱為作戰的作戰。
由於被我舉起來，眼前被切掉的那部分變為被拔出了的形式，眼前的【封魔之森】不見了。
因為是斜著切入的，切離拔掉後的【封魔之森】的地面變成四角錐了，最深的地方有數十米左右了。

「嗯......感覺不到重量啊......。」

手中握著被切下的【封魔之森】的一部分，我不由自主地嘟噥著。
試著將四角錐的前端放在右手掌上，卻感覺不到任何重量。喂，真的嗎？
話說，因為它的大小不大，把它放在指尖上也感覺不到重量。是什麼時候變成這樣了，我的身體啊。

『......』

我目瞪口呆地看著自己的身體。突然，我感覺到了視線，朝著那個方向一看，門衛和正門都變得目瞪口呆了。也是啊！做著這件事的我也是同樣的心境啊！
只是，這樣繼續拿著也不是辦法，所以我當場大聲向門衛問道。

「對不起！聽說這附近有海，是哪邊啊！？」
「......那，那邊......？」
「謝謝！」

巴爾夏帝國的士兵們雖然目瞪口呆，卻還是戰戰兢兢地指著某個方向。向他們道謝後，我拿著被切下來的【封魔之森】向著那個方向開始移動。

◆◇◆

『......』

誠一意氣風發地單手攜帶【封魔之森】的一部分離開，瓦爾夏帝國的門衛和正門除了啞然目送以外什麼都做不了。
與其這樣說，不如說對眼前一瞬間出現的現象，一點都不能理解。
誠一的身姿雖然看不見了，但是在遠處還能看到誠一所持有的【封魔之森】的一部分，現在仍在繼續移動。
大家都暫時凝視著這個不明所以的景象，不過，總算有一個門衛回過神來。
而且，這個門衛很幸運是能聚集其它門衛的人，所以他立刻下達了命令。

「哈！？喂，喂！現在馬上去城堡！向莉艾爾大人......不，直接向陛下彙報！」
「即使說要彙報......我該說什麼！？」
「那是......我不知道！」
「誒！？」
「事實就是這樣啊！？你說要怎麼彙報！」
「那隊長去彙報吧！」
「別說傻話了！一想到眼前成為客人的人突然飛了起來，突然在眼前出現了巨大的切塊，而且是那個客人舉起了它，然後就這樣搬走了......嗯，自己說著都莫名其妙了！好，睡吧！我可受不了！」

「喂，睡覺不好吧！？凱澤爾帝國的士兵──。」
「那些士兵好像都被運走了。」
「......。」

聽到身為門衛隊長說的話，他的部下默默向誠一拔掉的地方看去，確實看不到凱澤爾帝國的士兵的身影。

「──睡吧！」
「是吧！」

門衛們已經放棄了思考，索性以清爽的心情放棄工作。
但是，畢竟不是全員都能回去，也有幾個和正門一起留下來的士兵。
儘管如此，大多數守在門前的士兵，對眼前發生的難以置信的事情感到精神上的疲憊，就這樣回去了。
只是，作為最後的責任，雖然已經不知道怎樣傳達才好，不過，決定返回的士兵的每個人都為了告訴莉艾爾她們剛才的事向著城裡移動。

◆◇◆

「哦，真的是大海啊。」

多虧向門衛請教，平安穿過森林的我，看著眼前廣闊的大海點了點頭。

「好，總之，如果適當地飄浮到離這裡很遠的地方，就不會再來了吧。」

馬上，我由於『空王之靴』的效果浮上天空，就那樣輕鬆地向海的對面移動。但是，因為不知道要走到什麼時候，我在手中【封魔之森】的一部分不會受到影響的程度下輕輕地跑著。
然後，不愧是變成莫名其妙的身體能力的我，只要邁出一步，景色就會改變，回頭望去，已經看不到剛剛到達的瓦爾夏帝國的陸地。嗯，事到如今了啊。
儘管如此還是覺得不夠的我，在短暫的海上奔跑後，發現了漂浮著不祥的氣息的海的地方。
在我視野範圍內可以看到，那裡存在著許多巨大的漩渦，不知為何只有那裡的上空下著大雨和落雷。
而且，在漩渦群的正中間，有著從一開始就被準備好，適合我手上【封魔之森】的一部分的寬廣安靜的海面。

「恩，這裡就可以了吧？」

因為那裡的空間就好像在說，把手上的【封魔之森】的一部分放在那裡就好了。不，實際上並不是為了這個才準備的，不過機會難得。
我慢慢地在那裡把【封魔之森】的一部分放下，真的恰好陷入了漩渦群的中央。

「哦，真厲害。」

因為嵌合的太完美，我忍不住感動起來，亞美利亞她們也差不多要起床了所以我決定回去。

「嘛，這樣的話就不能去瓦爾夏帝國了吧。」

最後再看一眼【封魔之森】的一部分，在周邊的漩渦，我總覺得飄浮不安的氣息像巨大的龍的尾巴一樣的東西。還有，雖然不及我搬運的【封魔之森】的一部分，不過逼近那個大小的巨大的魚影到處都能看見。

「......嗯，回去吧。」

雖然不知道要怎麼從那裡逃出去，不過還是請你們加油吧。凱澤爾帝國的士兵都是『超越者』，總會有辦法的。木材也有很多，船不是隨便能做出來嗎。

「那麼，在這裡就可以使用魔法了吧......。」

因為現在已經不在【封魔之森】附近，使用轉移魔法應該可以回到莎莉亞她們身邊......。

「嘛，我也沒和亞美利亞打招呼，無論如何，我都要把我拔掉這片森林一部分的事處理掉。」

就這樣，我老實地返回到瓦爾夏帝國。
只是，出發的時候正好經過正門，如果我突然出現會嚇到她們的，用轉移魔法移動到能看見海和【封魔之森】的地方就好了吧？
就這樣決定後，我立刻發動了轉移魔法，我平安地回到了能看見【封魔之森】和海的地方。


